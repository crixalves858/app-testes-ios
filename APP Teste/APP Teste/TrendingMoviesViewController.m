//
//  TrendingMoviesViewController.m
//  APP Teste
//
//  Created by Cristiano Alves on 16/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import "TrendingMoviesViewController.h"
#import "APITrakt.h"
#import "Movie.h"
#import "MovieCellView.h"
#import "MovieViewController.h"
@interface TrendingMoviesViewController () <UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@end
@implementation TrendingMoviesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Trending Movies"];
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    self.refreshControl.attributedTitle=[[NSAttributedString alloc]initWithString:@"Loading"];
    
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl beginRefreshing];
    
    [APITrakt getTrendingMoviesSucess:^(id object){
        
        self.movies=(NSMutableArray *)object;
        
    } failure:requestError watched:NO];
    
}
-(void)refreshData
{
    
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
    
    
    
}

void (^requestError)() = ^void(NSError *error) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error"
                                                    message:@"Error in request"
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"Ok", nil];
    [alert show];
    
};

-(void)setMovies:(NSMutableArray *)movies{
    _movies=movies;
    [self refreshData];
    
}



- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:@"Mark"];
    return rightUtilityButtons;
}



#pragma mark - SWTableViewDelegate



- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            NSLog(@"Mark button was pressed");
            break;
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self movies] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *menuTableIdentifier = @"Movie Cell";
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuTableIdentifier];
    
    
    MovieCellView *cell = (MovieCellView *)[tableView dequeueReusableCellWithIdentifier:menuTableIdentifier];

    if (cell == nil) {
        cell = [[MovieCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:menuTableIdentifier];
    }
    
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;
    Movie *m=[self.movies objectAtIndex:[indexPath row]];
    cell.titleLabel.text=m.title;
    cell.subTitleLabel.text=m.tagLine;
    NSString *genres=@"";
    bool first=true;
    for (id s in m.genres) {
        if(first)
            genres=s;
        else
            genres=[NSString stringWithFormat:@"%@ %@",genres,s];
        first=false;
    }
    cell.footerLabel.text=genres;
    if(m.dataImage !=nil)
        cell.pictureImageView.image = [UIImage imageWithData:[m dataImage]];
    else{
        if(m.image != (id)[NSNull null])
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[m image]]];
                m.dataImage=imageData;
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.pictureImageView.image = [UIImage imageWithData:imageData];
                });
            });
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"Movie" sender:indexPath];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"Movie"]) {
        MovieViewController *movieViewController = [segue destinationViewController];
        movieViewController.movie=[self.movies objectAtIndex:[(NSIndexPath *)sender row]];
    }
}






@end
