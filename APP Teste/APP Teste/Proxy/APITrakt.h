//
//  Request.h
//  APP Teste
//
//  Created by Cristiano Alves on 16/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface APITrakt : NSObject

+(void) getTrendingMoviesSucess:(void (^)(id))sucess failure:(void(^)(NSError *error))failure watched:(BOOL)watched;

+(void) seenMovie:(void (^)(id))sucess failure:(void(^)(NSError *error))failure imbd_id:(NSString *)imdb_id;

+(void) getTrendingShowsSucess:(void (^)(id))sucess failure:(void(^)(NSError *error))failure watched:(BOOL)watched;

@end
