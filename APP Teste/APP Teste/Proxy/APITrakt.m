//
//  Request.m
//  APP Teste
//
//  Created by Cristiano Alves on 16/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import "APITrakt.h"
#import "Movie.h"
#import "Show.h"

#define URL_HOST @"http://api.trakt.tv/"
#define URL_GET_TRENDING_MOVIES @"movies/trending"
#define API_KEY @"124416d443e2f735e42fbd4b8353f400"
#define FORMAT @"json"
//https://dl.dropboxusercontent.com/u/5896294/filmes.json
#define URL_SEEN_MOVIE @"movie/seen"
#define URL_GET_TRENDING_SHOWS @"shows/trending"


@implementation APITrakt

+(void) requestGET:(NSString *)endPoint sucess:(void (^)(id responseObject))sucess failure:(void(^)(NSError *error))failure
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@%@.%@/%@",URL_HOST,endPoint,FORMAT,API_KEY]
       parameters:@{@"username": @"crixalves", @"password":@"cristiano"}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              sucess(responseObject);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(error.debugDescription);
              failure(error);
          }];

    
    
    
}

+(void) requestPOST:(NSString *)endPoint sucess:(void (^)(id responseObject))sucess failure:(void(^)(NSError *error))failure
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@%@/%@",URL_HOST,endPoint,API_KEY]
       parameters:@{@"username": @"crixalves", @"password":@"cristiano",@"imdb_id":@"tt2267998"}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              sucess(responseObject);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              failure(error);
          }];
    
    
    
    
}

+(void) seenMovie:(void (^)(id))sucess failure:(void(^)(NSError *error))failure imbd_id:(NSString *)imdb_id
{
    [self requestPOST:URL_SEEN_MOVIE sucess:^(id responseObject) {
        sucess(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
    
}

+(void) getTrendingMoviesSucess:(void (^)(id))sucess failure:(void(^)(NSError *error))failure watched:(BOOL)watched
{
    [self requestGET:URL_GET_TRENDING_MOVIES sucess:^(id responseObject) {
        NSArray *jsonAra=(NSArray *)responseObject;
        NSMutableArray *array=[[NSMutableArray alloc] init];
        for ( id object in jsonAra ) {
            Movie *m=[Movie parseMovie:object];
            if(m.watched==watched)
                [array addObject:m];
        }
        sucess(array);
    
    } failure:^(NSError *error) {
        failure(error);
    }];
}


+(void) getTrendingShowsSucess:(void (^)(id))sucess failure:(void(^)(NSError *error))failure watched:(BOOL)watched
{
    [self requestGET:URL_GET_TRENDING_SHOWS sucess:^(id responseObject) {
        NSArray *jsonAra=(NSArray *)responseObject;
        NSMutableArray *array=[[NSMutableArray alloc] init];
        for ( id object in jsonAra ) {
            Show *s=[Show parseShow:object];
            //if(m.watched==watched)
            [array addObject:s];
        }
        sucess(array);
        
    } failure:^(NSError *error) {
        failure(error);
    }];
}


@end
