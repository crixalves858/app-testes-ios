//
//  User.m
//  APP Teste
//
//  Created by Cristiano Alves on 13/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import "User.h"
#import <Parse/PFObject+Subclass.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

@implementation User
+ (void)load {
    [self registerSubclass];
}

+ (User *)currentUser {
    return (User *)[PFUser currentUser];
}

-(NSString *) facebookName{
    return self.facebookInfo[@"name"];
}
-(NSString *) facebookLocation{
    return self.facebookInfo[@"location"][@"name"];
}
-(NSURL *) facebookURLImage{
    NSString *facebookID = self.facebookInfo[@"id"];
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
}

- (void)loadFacebookData {
    FBRequest *request = [FBRequest requestForMe];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            
            NSDictionary *userData = (NSDictionary *)result;
            self.facebookInfo=userData;
            [self saveInBackground];
            
        }
    }];
    
}


@dynamic facebookInfo;
@end
