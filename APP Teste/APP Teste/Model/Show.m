//
//  Show.m
//  APP Teste
//
//  Created by Cristiano Alves on 02/01/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "Show.h"

@implementation Show

+(Show *)parseShow:(NSDictionary *)JSON{
    Show *s=[[Show alloc] init];
    s.title=JSON[@"title"];
    s.overview=JSON[@"overview"];
    s.year=[NSString stringWithFormat:@"%d",[JSON[@"year"] integerValue] ];
    s.image=JSON[@"images"][@"poster"];
    //s.watched=[JSON[@"watched"] boolValue];
    s.genres=[[NSMutableArray alloc]init];
    //s.imbd_id=JSON[@"imbd_id"];
    //s.dataImage=nil;
    NSArray *jsonAra=(NSArray *)JSON[@"genres"];
    for ( id object in jsonAra ) {
        [s.genres addObject:object];
    }
    return s;
}

@end
