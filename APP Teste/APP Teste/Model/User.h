//
//  User.h
//  APP Teste
//
//  Created by Cristiano Alves on 13/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface User : PFUser<PFSubclassing>
@property (retain) NSDictionary *facebookInfo;
@property (readonly) NSString *facebookName;
@property (readonly) NSString *facebookLocation;
@property (readonly) NSURL *facebookURLImage;
+ (User *)currentUser;
- (void) loadFacebookData;
@end
