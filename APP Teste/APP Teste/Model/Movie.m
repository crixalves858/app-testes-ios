//
//  Movie.m
//  APP Teste
//
//  Created by Cristiano Alves on 16/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import "Movie.h"

@implementation Movie

+(Movie *)parseMovie:(NSDictionary *)JSON{
    Movie *m=[[Movie alloc] init];
    m.title=JSON[@"title"];
    m.tagLine=JSON[@"tagline"];
    m.year=[NSString stringWithFormat:@"%d",[JSON[@"year"] integerValue] ];
    m.image=JSON[@"images"][@"poster"];
    m.watched=[JSON[@"watched"] boolValue];
    m.genres=[[NSMutableArray alloc]init];
    m.imbd_id=JSON[@"imbd_id"];
    m.dataImage=nil;
    NSArray *jsonAra=(NSArray *)JSON[@"genres"];
    for ( id object in jsonAra ) {
        [m.genres addObject:object];
    }
    return m;
}

@end
