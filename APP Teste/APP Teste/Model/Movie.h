//
//  Movie.h
//  APP Teste
	
//
//  Created by Cristiano Alves on 16/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *year;
@property (strong, nonatomic) NSString *tagLine;
@property (strong, nonatomic) NSString *image;
@property (nonatomic) BOOL watched;
@property (strong, nonatomic) NSMutableArray *genres;
@property (strong, nonatomic) NSString *imbd_id;
@property (strong, nonatomic) NSData *dataImage;
+(Movie *)parseMovie:(NSDictionary *)JSON;

@end
