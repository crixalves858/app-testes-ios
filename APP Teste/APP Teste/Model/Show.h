//
//  Show.h
//  APP Teste
//
//  Created by Cristiano Alves on 02/01/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Show : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *year;
@property (strong, nonatomic) NSString *overview;
@property (strong, nonatomic) NSMutableArray *genres;
@property (strong, nonatomic) NSString *image;
+(Show *)parseShow:(NSDictionary *)JSON;
@end
