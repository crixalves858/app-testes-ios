//
//  UIFont+MyFont.m
//  APP Teste
//
//  Created by Cristiano Alves on 29/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import "UIFont+MyFont.h"

@implementation UIFont (MyFont)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

+ (UIFont *)boldSystemFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"Gotham Bold" size:fontSize];
}

+ (UIFont *)systemFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"Gotham Black" size:fontSize];
}

+ (UIFont *)italicSystemFontOfSize:(CGFloat)fontSize{
    return [UIFont fontWithName:@"GothamBlack-Italic" size:fontSize];
}


#pragma clang diagnostic pop

@end
