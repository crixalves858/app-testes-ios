//
//  DrawMenuFactory.m
//  APP Teste
//
//  Created by Cristiano Alves on 13/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import "DrawMenuFactory.h"


@implementation DrawMenuFactory

+(MMDrawerController *) createMMDrawerController:(UIViewController *)centerView :(UIViewController *)leftView{
    UIViewController * leftSideDrawerViewController = leftView;
    
    UIViewController * centerViewController = centerView;
    
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:centerViewController];
    [navigationController setRestorationIdentifier:@"MMExampleCenterNavigationControllerRestorationKey"];
    MMDrawerController * drawerController = [[MMDrawerController alloc]
                             initWithCenterViewController:navigationController
                             leftDrawerViewController:leftSideDrawerViewController
                             ];
    [drawerController setRestorationIdentifier:@"MMDrawer"];
    [drawerController setMaximumRightDrawerWidth:200.0];
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    return drawerController;
}
@end
