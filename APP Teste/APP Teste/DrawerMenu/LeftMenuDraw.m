//
//  LeftMenuDraw.m
//  APP Teste
//
//  Created by Cristiano Alves on 12/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import "LeftMenuDraw.h"
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import <QuartzCore/QuartzCore.h>
#import "UIViewController+MMDrawerController.h"
#import "User.h"
@interface LeftMenuDraw ()

@property (weak, nonatomic) IBOutlet UIImageView *userPicture;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userLocation;
typedef struct MenuItem
{
    __unsafe_unretained NSString *name;
    int itemID;
    
} MenuItem;
@property (strong, nonatomic) NSMutableArray *menuItems;
@property (strong, nonatomic) NSArray *itemsName;
@end

@implementation LeftMenuDraw



-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateUI];
}

- (void)updateUI {
    
    User *currentUser = [User currentUser];
    self.userName.text=currentUser.facebookName;;
    self.userLocation.text=currentUser.facebookLocation;
    self.userPicture.layer.cornerRadius = 25;
    self.userPicture.clipsToBounds = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:currentUser.facebookURLImage];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.userPicture.image = [UIImage imageWithData:imageData];
            
        });
    });
}

-(NSArray *)itemsName
{
    return @[@"Home", @"To Watch", @"Trending Movies", @"Trending Shows"];
}

-(NSMutableArray *) menuItems
{
    NSMutableArray *array=_menuItems;
    if(array==nil)
    {
        array=[[NSMutableArray alloc] init];
        int i;
        for (i = 0; i < [self.itemsName count]; i++) {
            MenuItem m;
            m.itemID=i;
            m.name=[self.itemsName objectAtIndex:i];
            [array addObject:[NSValue value:&m withObjCType:@encode(struct MenuItem)]];
                  }
    }
    return array;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *menuTableIdentifier = @"Menu Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:menuTableIdentifier];
    }
    MenuItem m;
    [[self.menuItems objectAtIndex:indexPath.row] getValue:&m];
    cell.textLabel.text = m.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController * centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:
                                               [self.itemsName objectAtIndex:indexPath.item]];
    
    UINavigationController *nav =
    (UINavigationController *)self.mm_drawerController.centerViewController;
    [nav popToRootViewControllerAnimated:NO];
    if(indexPath.item!=0)
        [nav pushViewController:centerViewController animated:NO];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
}

@end
