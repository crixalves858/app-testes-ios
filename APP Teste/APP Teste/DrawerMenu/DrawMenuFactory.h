//
//  DrawMenuFactory.h
//  APP Teste
//
//  Created by Cristiano Alves on 13/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MMDrawerController.h>

@interface DrawMenuFactory : NSObject

+(MMDrawerController *)createMMDrawerController:(UIViewController *)centerView :(UIViewController *)leftView;

@end
