//
//  LeftMenuDraw.h
//  APP Teste
//
//  Created by Cristiano Alves on 12/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuDraw : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
