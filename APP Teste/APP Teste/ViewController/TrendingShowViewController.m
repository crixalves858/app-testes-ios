//
//  TrendingShowViewController.m
//  APP Teste
//
//  Created by Cristiano Alves on 02/01/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "APITrakt.h"
#import "TrendingShowViewController.h"
#import "Show.h"
#import "ShowCellView.h"

@interface TrendingShowViewController () <UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@end

@implementation TrendingShowViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Trending Shows"];
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    self.refreshControl.attributedTitle=[[NSAttributedString alloc]initWithString:@"Loading"];
    
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl beginRefreshing];
    
    [APITrakt getTrendingShowsSucess:^(id object){
        
        self.shows=(NSMutableArray *)object;
        
    } failure:requestError1 watched:NO];
    
}
-(void)refreshData
{
    
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
    
    
    
}

void (^requestError1)() = ^void(NSError *error) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request ERROR"
                                                    message:@"Error in request"
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"Ok", nil];
    [alert show];
    
};
-(void)setShows:(NSMutableArray *)shows
{
    _shows=shows;
    [self refreshData];
    
}







- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self shows] count];
}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:@"Mark"];
    return rightUtilityButtons;
}



#pragma mark - SWTableViewDelegate



- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            NSLog(@"Mark button was pressed");
            break;
        default:
            break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *menuTableIdentifier = @"Show Cell";
    
    ShowCellView *cell = (ShowCellView *)[tableView dequeueReusableCellWithIdentifier:menuTableIdentifier];
    
    if (cell == nil) {
        cell = [[ShowCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:menuTableIdentifier];
    }
    Show *show=[self.shows objectAtIndex:[indexPath row]];
    cell.delegate=self;
    cell.rightUtilityButtons = [self rightButtons];
    cell.labelTitle.text=show.title;
    cell.labelYear.text=show.year;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[show image]]];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.imageViewImage.image = [UIImage imageWithData:imageData];
        });
    });

    return cell;
}





@end
