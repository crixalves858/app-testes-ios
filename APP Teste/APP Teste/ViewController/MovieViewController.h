//
//  MovieViewController.h
//  APP Teste
//
//  Created by Cristiano Alves on 02/01/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"
@interface MovieViewController : UIViewController

@property (strong, nonatomic)Movie *movie;
@end
