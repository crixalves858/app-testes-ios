//
//  ViewController.m
//  APP Teste
//
//  Created by Cristiano Alves on 12/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import "LoginViewController.h"
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "MMDrawerController.h"
#import "LeftMenuDraw.h"
#import "CenterViewController.h"
#import "DrawMenuFactory.h"
#import "User.h"
#import "APITrakt.h"

@interface LoginViewController ()
@property (nonatomic,strong) MMDrawerController * drawerController;
@end

@implementation LoginViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)login_action:(id)sender {
    
    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];
    // Login PFUser using Facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
   
        
        if (!user) {
            NSString *errorMessage = nil;
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                errorMessage = @"Uh oh. The user cancelled the Facebook login.";
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
                errorMessage = [error localizedDescription];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error"
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Dismiss", nil];
            [alert show];
        } else {
            if (user.isNew) {
                NSLog(@"User with facebook signed up and logged in!");
            } else {
                NSLog(@"User with facebook logged in!");
            }
            User *currentUser = (User *)user;
            [currentUser loadFacebookData];
            [self openMenu];
        }
    }];
    
   
}



-(void) openMenu{
    UIViewController * leftSideDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Left Menu"];
    
    UIViewController * centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    
    MMDrawerController *menu=[DrawMenuFactory createMMDrawerController:centerViewController :leftSideDrawerViewController];
    
    [self presentViewController:menu animated:YES completion:nil];

}
@end
