//
//  TrendingShowViewController.h
//  APP Teste
//
//  Created by Cristiano Alves on 02/01/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "CenterViewController.h"

@interface TrendingShowViewController : CenterViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic)NSMutableArray *shows;

@end
