//
//  MovieCellView.m
//  APP Teste
//
//  Created by Cristiano Alves on 26/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import "MovieCellView.h"


@implementation MovieCellView

@synthesize titleLabel = _titleLabel;
@synthesize subTitleLabel = _subTitleLabel;
@synthesize footerLabel = _footerLabel;
@synthesize pictureImageView=_pictureImageView;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
