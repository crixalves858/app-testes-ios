//
//  MovieCellView.h
//  APP Teste
//
//  Created by Cristiano Alves on 26/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell.h>

@interface MovieCellView : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *footerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;

@end
