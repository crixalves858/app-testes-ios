//
//  MovieViewController.m
//  APP Teste
//
//  Created by Cristiano Alves on 02/01/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "MovieViewController.h"

@interface MovieViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *ImageView;
@property (weak, nonatomic) IBOutlet UILabel *labelYear;
@property (weak, nonatomic) IBOutlet UITextView *textGenres;

@property (weak, nonatomic) IBOutlet UITextView *textAbstract;

@end

@implementation MovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:[self.movie title]];
    // Do any additional setup after loading the view.
    [self updateUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateUI{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",@"Year:",[self.movie year]]];
    [str addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:NSMakeRange(0,5)];
 //   [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(6,2)];

    self.labelYear.attributedText=str;
    //self.labelYear.text=[self.movie year];
    NSString *genres=@"";
    bool first=true;
    for (id s in [self movie].genres) {
        if(first)
            genres=s;
        else
            genres=[NSString stringWithFormat:@"%@ %@",genres,s];
        first=false;
    }
    NSMutableAttributedString *strGenres = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",@"Genres:",genres]];
 //   [strGenres addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0,[strGenres length])];
    
  [strGenres addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:NSMakeRange(0,6)];
    
    self.textGenres.attributedText=strGenres;
    
    self.textAbstract.text= [self.movie tagLine];
    if(self.movie.dataImage !=nil)
        self.ImageView.image = [UIImage imageWithData:[self.movie dataImage]];
    else{
        if(self.movie.image != (id)[NSNull null])
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[self.movie image]]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.movie.dataImage=imageData;
                    self.ImageView.image = [UIImage imageWithData:imageData];
                });
            });
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
