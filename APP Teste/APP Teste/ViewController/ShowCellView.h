//
//  ShowCellView.h
//  APP Teste
//
//  Created by Cristiano Alves on 03/01/15.
//  Copyright (c) 2015 Cristiano Alves. All rights reserved.
//

#import "SWTableViewCell.h"

@interface ShowCellView : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewImage;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelYear;

@end
