//
//  TrendingMoviesViewController.h
//  APP Teste
//
//  Created by Cristiano Alves on 16/12/14.
//  Copyright (c) 2014 Cristiano Alves. All rights reserved.
//

#import "CenterViewController.h"

@interface TrendingMoviesViewController : CenterViewController  <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic)NSMutableArray *movies;

@end
